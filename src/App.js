import React from "react";
import { Routes, Route } from "react-router-dom"
import FixedNavbar from "./Components/FixedNavbar";
import { Index } from "./Routes";

function App() {
  return (
    <>
      <FixedNavbar />
      <Routes>
        <Route index element={<Index />} />
      </Routes>
    </>
  );
}

export default App;
