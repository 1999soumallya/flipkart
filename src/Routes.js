import Carousel from "./Components/Carousel"
import HomeNavbar from "./Components/HomeNavbar"

export const Index = () => {
    return (
        <>
            <HomeNavbar />
            <div className="body_container mx-2">
                <Carousel />
            </div>
        </>
    )
}