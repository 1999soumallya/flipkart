import React from 'react'
import ReactOwlCarousel from 'react-owl-carousel'
import { owlCarousel } from '../Settings'
import "../Css/Carousel.css"
import { Image } from 'react-bootstrap'

export default function Carousel() {
    return (
        <>
            <ReactOwlCarousel className='owl-theme custombuttondegine' {...owlCarousel}>
                <div className='item'>
                    <Image src={"https://rukminim1.flixcart.com/fk-p-flap/1688/280/image/c5918e488ff6b784.jpg?q=50"} />
                </div>
                <div className='item'>
                    <Image src={"https://rukminim1.flixcart.com/fk-p-flap/1688/280/image/c5918e488ff6b784.jpg?q=50"} />
                </div>
                <div className='item'>
                    <Image src={"https://rukminim1.flixcart.com/fk-p-flap/1688/280/image/c5918e488ff6b784.jpg?q=50"} />
                </div>
                <div className='item'>
                    <Image src={"https://rukminim1.flixcart.com/fk-p-flap/1688/280/image/c5918e488ff6b784.jpg?q=50"} />
                </div>
            </ReactOwlCarousel>
        </>
    )
}
