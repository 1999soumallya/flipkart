import React from 'react'
import { Container, Image, Nav, Navbar } from 'react-bootstrap'
import '../Css/HomeNavbar.css'

export default function HomeNavbar() {
    return (
        <>
            <Navbar bg="light" collapseOnSelect expand="lg" id='homenavbar'>
                <Container>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto w-100 justify-content-between">
                            <Nav.Item className='eFQ30H'>
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/mobile.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Mobiles</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="eFQ30H">
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/Grocery.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Grocery</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="eFQ30H">
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/Grocery.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Grocery</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="eFQ30H">
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/Grocery.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Grocery</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className='eFQ30H'>
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/mobile.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Mobiles</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="eFQ30H">
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/Grocery.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Grocery</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="eFQ30H">
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/Grocery.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Grocery</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="eFQ30H">
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/Grocery.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Grocery</div>
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="eFQ30H">
                                <Nav.Link href="https://www.flipkart.com/grocery-supermart-store?marketplace=GROCERY&fm=neo%2Fmerchandising&iid=M_409108c0-a83f-4896-90fc-f3fc21e7c44f_1_372UD5BXDFYS_MC.CBUR1Q46W5F1&otracker=hp_rich_navigation_1_1.navigationCard.RICH_NAVIGATION_Grocery_CBUR1Q46W5F1&otracker1=hp_rich_navigation_PINNED_neo%2Fmerchandising_NA_NAV_EXPANDABLE_navigationCard_cc_1_L0_view-all&cid=CBUR1Q46W5F1">
                                    <div className="_1mkliO">
                                        <div className="CXW8mj" style={{ height: 64, width: 64 }}>
                                            <Image loading="lazy" className="_396cs4 rounded-circle overflow-hidden" alt="Grocery" src="assets/image/Grocery.png" />
                                        </div>
                                    </div>
                                    <div className="xtXmba">Grocery</div>
                                </Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Navbar.Collapse>
                </Container >
            </Navbar >
        </>
    )
}
