import React from 'react'
import { Container, Nav, Navbar, Image, Form, Button, NavDropdown } from 'react-bootstrap'
import '../Css/FixedNavbar.css'

export default function FixedNavbar() {
    return (
        <>
            <Navbar bg="primary" variant="dark" id="mainnavbar">
                <Container fluid id='FixedNavbar'>
                    <div className="_1TmfNK"></div>
                    <Navbar.Brand href="#">
                        <Image src="assets/image/flipkart-plus_8d85f4.png" width={75} alt='Flipkart' title='Flipkart' className='_2xm1JU' />
                        <Nav.Link className="_21ljIi" href="/plus">
                            Explore {" "}
                            <span className="_2FVHGh">Plus</span>
                            <Image width={10} src="assets/image/plus_aef861.png" alt='Plus Flipkart' />
                        </Nav.Link>
                    </Navbar.Brand>
                    <Navbar.Collapse className="collapse" id="navbarSupportedContent">
                        <Nav className="me-auto mb-2 mb-lg-0 w-100">
                            <Nav.Item className="searchbar">
                                <Form className="me-auto w-100">
                                    <Form.Group controlId="formBasicEmail" className="d-flex">
                                        <Form.Control type="search" placeholder="Search for products, brands and more" />
                                        <Button className="L0Z3Pu" type="submit">
                                            <svg width="20" height="20" viewBox="0 0 17 18" className="" xmlns="http://www.w3.org/2000/svg">
                                                <g fill="#2874F1" fillRule="evenodd">
                                                    <path className="_34RNph" d="m11.618 9.897l4.225 4.212c.092.092.101.232.02.313l-1.465 1.46c-.081.081-.221.072-.314-.02l-4.216-4.203"></path>
                                                    <path className="_34RNph" d="m6.486 10.901c-2.42 0-4.381-1.956-4.381-4.368 0-2.413 1.961-4.369 4.381-4.369 2.42 0 4.381 1.956 4.381 4.369 0 2.413-1.961 4.368-4.381 4.368m0-10.835c-3.582 0-6.486 2.895-6.486 6.467 0 3.572 2.904 6.467 6.486 6.467 3.582 0 6.486-2.895 6.486-6.467 0-3.572-2.904-6.467-6.486-6.467"></path>
                                                </g>
                                            </svg>
                                        </Button>
                                    </Form.Group>
                                </Form>
                            </Nav.Item>
                            <Nav.Item className="d-flex justify-content-between align-items-center text-center">
                                <Nav.Item>
                                    <Nav.Link className="_1_3w1N" id='loginNavbutton' href="/account">Login</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link className='_3-PJz-' href="https://seller.flipkart.com/sell-online/?utm_source=fkwebsite&amp;utm_medium=websitedirect">
                                        <span>Become a Seller</span>
                                    </Nav.Link>
                                </Nav.Item>
                                <NavDropdown title="More" id="basic-nav-dropdown">
                                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">
                                        Another action
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href="#action/3.4">
                                        Separated link
                                    </NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Item>
                                    <Nav.Link className='_3SkBxJ'>
                                        <svg className="V3C5bO" width="14" height="14" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                            <path className="_1bS9ic" d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86" fill="#fff"></path>
                                        </svg>
                                        <span>Cart</span>
                                    </Nav.Link>
                                </Nav.Item>
                            </Nav.Item>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}
