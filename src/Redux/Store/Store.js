import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

const reducer = combineReducers({

})

const initialState = {

}

const middlewere = [thunk]

export const store = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(...middlewere)))