export const owlCarousel = {
    loop: true,
    margin: 10,
    nav: true,
    items: 1,
    dots: false,
    autoplay: true,
    autoplayTimeout: 2000,
    smartSpeed: 300,
    navText: [
        '<img src="assets/image/previous.png">',
        '<img src="assets/image/next.png" />'
    ]
}